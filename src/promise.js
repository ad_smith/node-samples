/*
This example shows how promises work in conjunction with async/await in
top-level function calls.
*/
// Example long-running function
function getValue(cb) {
    console.log("initializing long-running process...")
    setTimeout(() => {
        console.log("long-running process finished")
        cb(8434)
    }, 1000)
}

// Wrap the long-running function as a promise
function getValuePromise() {
    return new Promise((resolve, reject) => {
        getValue((val) => {
            console.log("promise cb received", val)
            if (val > 1000) {
                resolve(val)
            }
            else {
                reject("rejected")
            }
        })
    })
}

// Use async/await for the promise
async function getValueAsync() {
    try {
        const val = await getValuePromise()
        console.log("resolved", val)
        return val
    }
    catch (e) {
        console.log("caught", e)
    }
}


// How to invoke the async/await function
getValueAsync()

// How to use the promise function
// getValuePromise().then(val => {
//     console.log("from promise", val)
// }, error => {
//     console.log("from promise error", error)
// })

// This always prints first
console.log("exiting")



