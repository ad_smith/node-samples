const pgp = require("pg-promise")()
const path = require("path")
const QueryFile = require("pg-promise").QueryFile

function sql(file) {
    const fullPath = path.join(__dirname, file)
    return new QueryFile(fullPath, { minify: true })
}

/* Without specifying the user and password, process.env.PGUSER and 
process.env.PGPASSWORD will be used to connect */
const cn = {
    host: "localhost",
    port: 5432,
    database: "sample",
}

const db = pgp(cn)
console.log(db.$cn)

const queryFile = sql("sql/query.sql")

async function f() {
    try {
        // Inline SQL
        // const result = await db.any("select current_user")
        // const result = await db.any("select * from sample_table")
        // File-based SQL
        const result = await db.any(queryFile)
        console.log(result)
        return result
    } catch (e) {
        console.log(e)
    }
}

/* Demonstrates how to use a transaction and rollbacks */
async function txExample() {
    try {
        await db.tx(async t => {
            await t.result("insert into sample_table values('hello world'")
            await t.result("insert into sample_table values('hello world'")
            throw new Error("error triggering rollback")
        })
    }
    catch (e) {
        console.log("Error received", e)
    }
}

async function runExamples() {
    await f()
    await txExample()
    pgp.end()
    console.log("Execution completed successfully")
}

runExamples()

