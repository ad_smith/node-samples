const express = require("express")
const server = express()
const port = 8000


server.get("/hellojson", (req, res) => {
    // Sends a response with Content-Type: application/json
    res.json({ message: "Hello world" })
})

server.get("/hello", (req, res) => {
    // Sends a response with Content-Type: text/html
    console.log(`Received GET request with query parameters ${JSON.stringify(req.query)}`)
    let name = req.query.name
    if (!req.query.name) {
        name = "world"
    }
    res.send(`Hello ${name}`)
})

server.listen(port, () => {
    console.log(`Server listening at port ${port}`)
})

