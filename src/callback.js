/* Makes a callback to he given function with the given value after a delay */
function callbackWithValue(val, cb) {
    console.log("initializing long-running process...")
    setTimeout(() => {
        console.log("long-running process finished")
        cb(val)
        console.log("callback complete")
    }, 1000)
}

module.exports = {
    callbackWithValue
}