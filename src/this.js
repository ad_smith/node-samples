// This file shows examples of how `this` works in functions and top-level code
global.a = 0
function f() {
    // Within a function invoked as f(), `this` is equivalent to `global`
    console.log("this within f", this.a) // Logs 0 when invoked as f()
    console.log("global within f", global.a) // Logs 0 when invoked as f()
    this.a = 1
    console.log("global within f", global.a) // Logs 1 when invoked as f() 
    global.a = 2
    console.log("this within f", this.a) // Logs 2 when invoked as f()
}

f()
this.a = 3

console.log("top-level global", global.a) // Logs 2
console.log("top-level this", this.a) // Logs 3

// In top-level code, `this` is equivalent to module.exports
this.b = 4
module.exports.c = 5
console.log(module.exports) // Logs { a: 3, b: 4, c: 5 }
console.log(this) // Also logs { a: 3, b: 4, c: 5 }

/* Note that the value of `this` within a function changes depending on how f
   is executed */
f.call({ a: 6 }) // Passes the anonymous object wih a === 6 to f as `this`
