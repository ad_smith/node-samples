/*
This example is meant to be used in conjunction with promise2.test.js to
demonstrate how promise callbacks work and how they can be tested with sinon and
proxyquire.
*/
const callbackWithValue = require("./callback").callbackWithValue

// Wrap the long-running function as a promise
function getValuePromise(myValue) {
    return new Promise((resolve, reject) => {
        callbackWithValue(myValue, (val) => {
            console.log("callback function received", val)
            if (val > 1000) {
                resolve(val)
            }
            else {
                reject(Error(`val is ${val}`))
            }
        })
    })
}

module.exports = getValuePromise
