
// A generator is specified by the *
function* sayHelloWorld() {
    console.log("Hello")
    yield
    console.log("world")
    // yield can return values as well as receive valuesS
    const name = yield "Good"
    console.log("bye", name)
}

// sayHelloWorld is a generator
console.log(typeof (sayHelloWorld))
// Invoking it returns a Generator object
const generator = sayHelloWorld()
console.log(generator)
// next() triggers the generator to execute 
generator.next()
let ret = generator.next()
console.log(ret.value, ret.done)
ret = generator.next("John")
// After the generator completes, ret.done === true
console.log(ret.done)

// Generator function for the Fibonacci sequence
function* fib() {
    let a = 0
    let b = 1
    while (true) {
        yield a
        let sum = a + b
        a = b
        b = sum
    }
}

const fibGenerator = fib()
for (let i = 0; i < 10; i++) {
    console.log(fibGenerator.next().value)
}

