const assert = require("assert")
const math = require("../src/math.js")


describe("math.js", () => {
    it("calculates gcd", () => {

        assert.equal(2, math.gcd(8, 34))
        assert.equal(21, math.gcd(1071, 462))
        assert.equal(21, math.gcd(-1071, -462))
        assert.equal(21, math.gcd(-1071, 462))
        assert.equal(21, math.gcd(-1071, -462))
    })
    it("calculates lcm", () => {

        assert.equal(32, math.lcm(16, 32))
        assert.equal(136, math.lcm(8, 34))
        assert.equal(23562, math.lcm(1071, 462))
        assert.equal(712, math.lcm(-8, 89))

    })
})
